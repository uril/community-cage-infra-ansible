import dnf
from dnfpluginsextras import _, logger
import subprocess


class RTPerlRestart(dnf.Plugin):
    name = 'rt_perl_restart'
    need_httpd_restart = False

    def __init__(self, base, cli):
        super(RTPerlRestart, self).__init__(base, cli)
        self.base = base
        self.cli = cli

    def resolved(self):
        tx = self.base.transaction
        if tx.install_set:
            for pkg in tx.install_set:
                if pkg.name == 'rt' or pkg.name.startswith('perl'):
                    self.need_httpd_restart = True
                    break

    def transaction(self):
        if self.need_httpd_restart:
            logger.info(_("Restarting httpd because either RT of Perl packages were installed/upgraded"))
            subprocess.call(['systemctl', 'restart', 'httpd'])

