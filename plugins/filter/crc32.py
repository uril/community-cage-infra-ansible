# until https://github.com/ansible/ansible/pull/17178 is merged

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

import binascii


# these function only accept BASE64 encoded strings

def crc32(s):
    return binascii.crc32(binascii.a2b_base64(s)) & 0xffffffff

def crc32hex(s):
    return hex(crc32(s))



class FilterModule(object):
    def filters(self):
        return {
            'crc32': crc32,
            'crc32hex': crc32hex
        }

