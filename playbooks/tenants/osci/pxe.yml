---

- hosts: www.osci.io
  vars:
    vg_name: "data"
    pxe_lv_name: "pxe"
    fs_type: xfs
  tasks:
  - name: Test if partition for extra data exists
    stat:
      path: /dev/vdb1
    register: data_1
  - name: Partition disk for extra data
    parted:
      device: /dev/vdb
      label: gpt
      number: 1
      name: "data_1"
      flags: [ lvm ]
      state: present
    when: not data_1.stat.exists
  - name: Create LVM VG for extra data
    lvg:
      vg: "{{ vg_name }}"
      pvs: "/dev/vdb1"
      state: present
  # if we need more, adding a new PV in the VG would be very easy
  - name: Create LVM LV for PXE data
    lvol:
      lv: "{{ pxe_lv_name }}"
      vg: "{{ vg_name }}"
      size: "100%VG"
      state: present
  - name: Create filesystem for PXE data
    filesystem:
      fstype: "{{ fs_type }}"
      dev: "/dev/{{ vg_name }}/{{ pxe_lv_name }}"
  - name: Mount filesystem for PXE data
    mount:
      src: "/dev/{{ vg_name }}/{{ pxe_lv_name }}"
      path: "{{ auto_inst_path }}"
      fstype: "{{ fs_type }}"
      state: mounted
  tags: partitioning

- hosts: www.osci.io
  vars:
    ## TEST
    #hostname: fedcloud-node-1.osci.io
    #root_pw: "toto123"
    #hash: fedcloudnode1.os
    auto_inst_vhost: "{{ auto_inst_url | urlsplit('hostname') }}"
  tasks:
    - name: "Create vhost for {{ auto_inst_vhost }}"
      include_role:
        name: httpd
        tasks_from: vhost
      vars:
        website_domain: "{{ auto_inst_vhost }}"
        document_root: "{{ auto_inst_path }}"
        use_tls: True
        use_letsencrypt: True
        force_tls: True
        content_security_policy: "default-src 'none'; font-src 'self'; img-src 'self'; style-src 'self' 'unsafe-inline'"
#    # this KS is experimental, but this is an example on how to populate preseeding materials
#    - name: Generate kickstart for Fedora Atomic
#      template:
#        src: "{{ inventory_dir }}/data/tenants/osci/pxe/fedora_atomic.ks"
#        dest: "{{ auto_inst_path }}/"
#        owner: root
#        group: root
#        mode: 0644
#      tags: pxe_atomic
  tags: pxe

- hosts: speedy.osci.io
  tasks:
    - name: Install DHCP Server
      include_role:
        name: dhcpd
      vars:
        dns_servers: "{{ osci_nameservers }}"
        smtp_server: "{{ mail_forwarder }}"
        ntp_servers: "{{ groups['ntp_servers'] }}"
        subnets:
          - subnet: "{{ cage_vlans['OSCI-Provisioning'].subnet }}"
            routers:
              - 172.24.30.254
            domain: "prov.osci.io"
            domain_search_list:
              - osci.io
              - redhat.com
            pxe:
              tftp_server: "{{ ansible_vlan430.ipv4.address }}"
              filename: "/pxelinux.0"
      tags: dhcp
    - name: Install TFTP Server
      include_role:
        name: tftp
      vars:
        allowed_subnets:
          - "{{ cage_vlans['OSCI-Management'].subnet }}" # to save device configs
          - "{{ cage_vlans['OSCI-Provisioning'].subnet }}"
      tags: tftp
    # it would be easily possible to add tenants keys for specific installations
    - name: Fetch SSH keys for post unattended installation access
      set_fact:
        sshkeys: "{{ tenant_groups['osci'] | map('extract', tenant_users) | sum(attribute='ssh_keys', start=[]) | list }}"
      tags: pxe_data
    - name: Prepare PXE Data
      include_role:
        name: pxe
        apply:
          tags:
            - pxe
            - pxe_data
      vars:
        vnc_passwd: "{{ pxe.vnc_passwd }}"
        # this does not work yet, see: https://gitlab.com/osas/ansible-role-pxe/merge_requests/4
        unattended_install:
          data:
            node: www.osci.io
            path: "{{ auto_inst_path }}"
            url: "{{ auto_inst_url }}"
          secrets:
            node: speedy.osci.io
            path: /etc/pxe_unattended_install
        distributions:
          CentOS:
            7_auto:
              base_version: 7
              architectures: [x86_64]
              comment: "(Unattended)"
              unattended_install: {}
          Fedora:
            30_atomic:
              base_version: 30
              architectures: [x86_64]
              comment: "(Atomic)"
              unattended_install:
                ostree: True
                # using the official repository is punishingly slow and all attempts failed
                # TODO: mirroring of the Atomic tree is done manually
                ostree_base_url: "{{ auto_inst_url }}/atomic/"
          RHEL:
            7_auto:
              base_version: 7
              architectures: [x86_64]
              comment: "(Unattended)"
              unattended_install: {}
# could be helpful because unattended installation is not working yet
#            27_atomic_cust:
#              base_version: 27
#              architectures: [x86_64]
#              comment: "(Atomic)"
#              #unattended_install: fedora_atomic.ks
#              append: "inst.ks=https://pxe.osci.io/Fedora_27_atomic.install.auto_x86_64.ks"
      tags: pxe_data
  tags: pxe

